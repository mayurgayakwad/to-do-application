const inputText = document.querySelector('input') 
let appendHolder = document.querySelector(".addition-to-do-list")
// create new to do list

function createParent() {
    
    let parentDiv = document.createElement("div")
    let tickButton = document.createElement("input")
    tickButton.setAttribute("type","checkbox")  
    let todoholder = document.createElement("p")
    let deleteButton = document.createElement('button')
    parentDiv.className = "parent"
    tickButton.className="checkbtn"
    todoholder.className = "toChange"
    todoholder.contentEditable = "true"
    deleteButton.className= "deletebnt"
    deleteButton.innerText = "X"
    tickButton.innerText = ""
    todoholder.innerText = inputText.value
    appendHolder.appendChild(parentDiv)
    parentDiv.append(tickButton)
    parentDiv.append(todoholder)
    parentDiv.append(deleteButton)
    inputText.value = ''

    let numb = document.getElementById("container").children.length;
    if(numb > 1){
        const actionDiv = document.getElementById("actionBar")
        actionDiv.style.display= "flex"
    }
    itemLeftCounter()
}

    // if press enter key new todoAdd
inputText.addEventListener("keypress", (holder) => {
    if(holder.key === "Enter" && inputText.value){    
        createParent()
        
    } })
        
let deletebtn = document.querySelector('.addition-to-do-list')
deletebtn.addEventListener("click" , (e) => {
    if(e.target.className == "deletebnt"){
        let parent = e.target.parentElement
        parent.remove()
        itemLeftCounter()
    }
    if(e.target.className == "checkbtn"){
        let child = e.target.parentElement
        let btn = child.querySelector(".toChange")
        child.classList.toggle("completed")
        child.classList.toggle("check")
        btn.classList.toggle('check-btn')
        let chkbox = child.querySelector('.checkbtn')
        itemLeftCounter()

    }
    
    let numb = document.getElementById("container").children.length;
        if(numb == 1){
            const actionDiv = document.getElementById("actionBar")
            actionDiv.style.display= "none"
        }
})



// Active Button

let activeBtn = document.querySelector(".active-btn")

activeBtn.addEventListener("click" ,(e) => {
    document.querySelectorAll(".parent").forEach((ele)=>{
        ele.style.display = "flex"
        
    })
    document.querySelectorAll('.completed').forEach((ele) => {
        ele.style.display = "none"
    })
      
})


// All button

let allBtn = document.querySelector(".all-btn")
allBtn.addEventListener("click", (e)=>{
    document.querySelectorAll(".parent").forEach((ele)=>{
        ele.style.display = "flex"
        
    })
})


// completed task button
let completeBtn = document.querySelector(".complete-btn")
completeBtn.addEventListener("click", (e)=>{
    document.querySelectorAll(".check").forEach((ele)=>{
        if(ele.className === "parent completed check")
        ele.style.display = "flex"
        
    })
})
completeBtn.addEventListener("click", (e)=>{
    document.querySelectorAll(".parent").forEach((ele)=>{
        if(ele.className === "parent"){
            ele.style.display = "none"
        }
        
    })
})


// Clear Completed task Button
let clearcompleted = document.querySelector(".clear-completed")
clearcompleted.addEventListener("click" ,(e) => {
        document.querySelectorAll('.completed').forEach((ele) => {
            ele.remove()
        })
})

// Counter for item count


function itemLeftCounter(){
    let unCompleted = document.querySelectorAll(".parent")
    let completed=document.querySelectorAll(".check")
    document.querySelector(".Item-left-counter").innerText=  "Left-Item : " + `${ unCompleted.length - completed.length}`
    
}

